from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json


from common.json import ModelEncoder
from .models import Salesperson, Customer, Sale, AutomobileVO



class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        # "import_href",
        "vin",
        "sold",
    ]

class SalespeopleEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
        "automobile",
        "salesperson",
        "customer",
    ]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "salesperson": SalespeopleEncoder(),
        "customer": CustomerEncoder(),
    }
    def get_extra_data(self, o):
        return {
            "automobile": o.automobile.vin,
            "salesperson_id": o.salesperson.employee_id,
            "salesperson": f"{o.salesperson.first_name} {o.salesperson.last_name}",
            "customer": f"{o.customer.first_name} {o.customer.last_name}",
            "sold": o.automobile.sold,
            "salespersonID": o.salesperson.id
            }


# SALESPERSON VIEWS =====================================

@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":

        salespeople = Salesperson.objects.all()

        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespeopleEncoder,
        )
    else:
        try:
            content = json.loads(request.body)

            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespeopleEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create salesperson"}
            )
            response.status_code=400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def show_salespeople(request, pk):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.get(id=pk)
            return JsonResponse(
                salespeople,
                encoder=SalespeopleEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code=404
            return response
    elif request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespeopleEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code=404
            return response


# CUSTOMER VIEWS ======================================


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create customer"}
            )
            response.status_code=400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def show_customers(request, pk):
    if request.method == "GET":
        try:
            customers = Customer.objects.get(id=pk)
            return JsonResponse(
                customers,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code=404
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code=404
            return response


# SALES VIEWS =========================================


@require_http_methods(["GET", "POST"])
def list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales":sales},
            encoder=SaleEncoder
        )
    else:
        content = json.loads(request.body)
        try:

            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson

            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer

            auto_id = content["automobile"]
            auto = AutomobileVO.objects.get(id=auto_id)
            content["automobile"] = auto

            auto.sold = True
            auto.save()

            sale = Sale.objects.create(**content)

            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False
            )

        except (AutomobileVO.DoesNotExist, Salesperson.DoesNotExist, Customer.DoesNotExist):
            return JsonResponse(
                {"message": "Invalid data provided"},
                status=400,
            )


@require_http_methods(["DELETE", "GET", "PUT"])
def show_sales(request, pk):
    if request.method == "GET":
        try:
            sales = Sale.objects.get(id=pk)
            return JsonResponse(
                sales,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code=404
            return response
    elif request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code=404
            return response
