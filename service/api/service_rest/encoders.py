from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "id",
      
       
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }
    def get_extra_data(self, o):
        
        is_vip_result = o.get_is_vip()
        return {"is_vip":is_vip_result, "technician":o.technician.first_name}

   
        
class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "id", "sold"]

  