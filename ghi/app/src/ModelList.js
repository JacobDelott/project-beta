import React, { useState, useEffect } from "react";
import "./CustomCSS.css";


function ModelList(props) {
  const [models, setModels] = useState([]);

  const fetchData = async () => {
    const response = await fetch("http://localhost:8100/api/models/");
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <h1>Models</h1>
      <table className="table table-striped table-color">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {models?.map((model) => {
            return (
              <tr key={model.id}>
                <td>{model.name}</td>
                <td>{model.manufacturer}</td>
                <td>
                  <img
                    className="auto-model-list-img"
                    src={model.picture_url}
                  />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ModelList;
