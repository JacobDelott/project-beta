import React, { useState, useEffect } from 'react';
import "./CustomCSS.css";


function BySalespersonList() {
    const [sales, setSales] = useState([])
    const [salespeople, setSalespeople] = useState([])
    const [selectSalesperson, setSelectSalesperson] = useState('')

    const handleSalespersonChange = (event) => {
        setSelectSalesperson(event.target.value)
    }


// selectSalesperson is the string of selected value
// ? = if true (value has been selected), do everything before : filter the array
//        compare sale.salespersonID with integer version of selectSalesperson
//    if false, do everything after : (show all of sales list)

    const filteredSales = selectSalesperson
        ? sales.filter((sale) => sale.salespersonID === parseInt(selectSalesperson))
        : sales



 // FETCH ===============================================================
    const fetchSalesperson = async () => {
        const response = await fetch("http://localhost:8090/api/salespeople/")
        if (response.ok) {
            const data = await response.json()
            setSalespeople(data.salespeople)
        }
    }

    const fetchSales = async () => {
        const response = await fetch("http://localhost:8090/api/sales/")
        if (response.ok) {
            const data = await response.json()
            setSales(data.sales)
        }
    }


    useEffect(() => {
    fetchSales()
    fetchSalesperson()
    }, [])


    return (
        <div>
        <h1>Salesperson History</h1>
        <div className="mb-3">
                <select
                    value={selectSalesperson}
                    onChange={handleSalespersonChange}
                    required id="selectSalesperson"
                    name="selectSalesperson"
                    className="form-select">
                  <option value="">Select a salesperson</option>
                  {salespeople?.map(salesperson => {
                    return (
                        <option key={salesperson.id} value={salesperson.id}>
                            {salesperson.first_name} {salesperson.last_name}
                        </option>
                    );
                  })
                  }
                </select>
                </div>
    <table className="table table-striped table-color">
      <thead>
        <tr>
          <th>Salesperson Name</th>
          <th>Customer</th>
          <th>VIN</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>
        {filteredSales?.map((sale) => {
            return (
            <tr key={sale.id}>
                <td>{ sale.salesperson }</td>
                <td>{ sale.customer } </td>
                <td>{ sale.automobile }</td>
                <td>{ sale.price }</td>
            </tr>
        );
        })}
      </tbody>
    </table>
  </div>
    )
}

export default BySalespersonList;
