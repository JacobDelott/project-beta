import { useState, useEffect } from "react";
import "./CustomCSS.css";


function TechniciansList() {
  const [technicians, setTechnicians] = useState([]);
  const fetchData = async () => {
    const response = await fetch("http://localhost:8080/api/technicians/");
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
      console.log(data.technicians);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <h1>Technicians</h1>
      <table className="table table-striped table-color">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>employee id</th>
          </tr>
        </thead>
        <tbody>
          {technicians?.map((technician) => {
            return (
              <tr key={technician.id}>
                <td>{technician.first_name}</td>
                <td>{technician.last_name} </td>
                <td>{technician.employee_id}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default TechniciansList;
