import React, { useState, useEffect } from 'react';
import "./CustomCSS.css";


function SalespeopleList() {
    const [salespeople, setSalespeople] = useState([])

    const fetchData = async () => {
        const response = await fetch("http://localhost:8090/api/salespeople/")
        if (response.ok) {
            const data = await response.json()
            setSalespeople(data.salespeople)
        }
    }

    useEffect(() => {
    fetchData()
    }, [])



    return (
        <div>
        <h1>Salespeople</h1>
    <table className="table table-striped table-color">
      <thead>
        <tr>
          <th>EmployeeID</th>
          <th>First Name</th>
          <th>Last Name</th>
        </tr>
      </thead>
      <tbody>
        {salespeople?.map((salesperson => {
            return (
            <tr key={salesperson.id}>
                <td>{ salesperson.employee_id }</td>
                <td>{ salesperson.first_name }</td>
                <td>{ salesperson.last_name } </td>
            </tr>
        );
        }))}
      </tbody>
    </table>
  </div>
    )
}

export default SalespeopleList;
