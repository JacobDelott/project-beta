import { useEffect, useState } from "react";
import "./CustomCSS.css";


function AppointmentsList(props) {
  const filtersList = props.filtersList;
  function isDefaultFilter(filtersList) {
    if (
      filtersList.length === 0 ||
      (filtersList.length === 1 && filtersList[0] === "")
    ) {
      return true;
    }
    return false;
  }
  function filteredAppointmentsList(appointmentsList, allowedStatuses) {
    if (appointmentsList.length === 0) {
      return appointmentsList;
    }
    if (isDefaultFilter(allowedStatuses)) {
      return appointmentsList.filter(
        (appointment) => appointment.status === ""
      );
    } else {
      return appointmentsList.filter((appointment) => {
        return !!allowedStatuses.find(
          (statusFilter) => appointment.status === statusFilter
        );
      });
    }
  }

  async function handleAppointmentStatusChange(event) {
    const newStatus = event.target.innerText;
    const appointmentId = parseInt(
      event.target.getAttribute("data-appointment-id")
    );
    try {
      const response = await fetch(
        `http://localhost:8080/api/appointments/${appointmentId}/${newStatus}/`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
          },
          mode: "cors",
          body: JSON.stringify({}),
          // autorefresh - delete from page
        }).then((result) => {
          if (result.ok) {
              fetchData();

        }}
      );
      const result = await response.json();
    } catch (error) {
      console.log("ERROR: ", error);
    }
  }

  const [appointments, setAppointments] = useState([]);
  const fetchData = async () => {
    const response = await fetch("http://localhost:8080/api/appointments/");
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
      console.log(data.appointments);
    }
  };

  function parseDateAndTime(date_time) {
    const dt = new Date(date_time);
    return [dt.toLocaleDateString(), dt.toLocaleTimeString()];
  }

  useEffect(() => {
    fetchData();
  }, []);

  console.log(
    "filtered appointments",
    filteredAppointmentsList(appointments, filtersList)
  );

  return (
    <div>
      <h1>
        {isDefaultFilter(filtersList)
          ? "Service Appointments"
          : "Service History"}
      </h1>
      {/* <input
        type="text"
        // value={searchQuery}
        // onChange={handleSearch}
        placeholder="Search..."
      /> */}

      {/* <input type="text" class="search form-control" placeholder="What you looking for?" />
      <button type="submit" className="create-button right-side">Search</button> */}

      <div className="row">
      <div className="col">
        <input type="text" className="search form-control spacing" placeholder="What you looking for?" />
      </div>
      <div className="col-auto">
        <button type="submit" className="btn create-button">Search</button>
      </div>
    </div>

      <table className="table table-striped table-color table-color">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>{isDefaultFilter(filtersList) ? "" : "Status"}</th>
          </tr>
        </thead>
        <tbody>
          {filteredAppointmentsList(appointments, filtersList)?.map(
            (appointment) => {
              const [date, time] = parseDateAndTime(appointment.date_time);
              return (
                <tr key={appointment.id}>
                  <td>{appointment.vin}</td>
                  <td>{appointment.is_vip}</td>

                  <td>{appointment.customer} </td>
                  <td>{date}</td>
                  <td>{time}</td>
                  <td>{appointment.technician}</td>
                  <td>{appointment.reason}</td>
                  <td>
                    {isDefaultFilter(filtersList) ? (
                      <>
                        <button
                          onClick={handleAppointmentStatusChange}
                          className="btn btn-sm cancel-button"
                          data-appointment-id={appointment.id}
                        >
                          cancel
                        </button>

                        <button
                          onClick={handleAppointmentStatusChange}
                          className="btn btn-sm finish-button"
                          data-appointment-id={appointment.id}
                        >
                          finish
                        </button>
                      </>
                    ) : (
                      appointment.status
                    )}
                  </td>
                </tr>
              );
            }
          )}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentsList;

// {
// 			"date_time": "2023-07-26T00:02:02.921050+00:00",
// 			"reason": "oil change",
// 			"status": "finished",
// 			"vin": "AB6CD2345EF678GHI",
// 			"customer": "Jake Delott",
// 			"id": 2,
// 			"appointment": "Lisa"
