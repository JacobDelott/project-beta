import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./CustomCSS.css";


function CreateServiceAppointmentForm() {
  const [automobileVin, setAutomobileVin] = useState("");
  const [customer, setCustomer] = useState("");

  // const [employeeId, setEmployeeId] = useState("");
  const [dateTime, setDateTime] = useState("");

  const [technician, setTechnician] = useState("");
  const [reason, setReason] = useState("");
  const [technicians, setTechnicians] = useState([]);
  const navigate = useNavigate("");

  const createAppointment = async (appointment) => {
    const url = "http://localhost:8080/api/appointments/";
    const response = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(appointment),
    });
    if (response.ok) {
      const data = await response.json();
      console.log(data);
      navigate("/appointments");
    }
  };

  const fetchTechnicians = async () => {
    const response = await fetch("http://localhost:8080/api/technicians/");
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  useEffect(() => {
    fetchTechnicians();
  }, []);

  function handleChangeAutomobileVin(event) {
    const value = event.target.value;
    setAutomobileVin(value);
  }
  function handleChangeCustomer(event) {
    const value = event.target.value;
    setCustomer(value);
  }

  // function handleChangeEmployeeId(event) {
  //   const value = event.target.value;
  //   setEmployeeId(value);
  // }
  function handleChangeDateTime(event) {
    const value = event.target.value;
    setDateTime(value);
  }

  function handleChangeTechnician(event) {
    const value = parseInt(event.target.value);
    const newTechnician = technicians.find(
      (technician) => technician.id === value
    );
    console.log(newTechnician);
    setTechnician(newTechnician);
  }
  // function handleChangeTechnicians(event) {
  //   const value = event.target.value;
  //   setTechnicians(value);
  // }
  function handleChangeReason(event) {
    const value = event.target.value;
    setReason(value);
  }
  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      vin: automobileVin,
      customer,
      date_time: dateTime,

      technician: technician.id,
      reason,
    };

    createAppointment(data);
  };
  return (
    <div>
      <form onSubmit={handleSubmit} id="create-appointment-form">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4 card-color">
              <h1 className="card-title">Add a Service Appointment</h1>
              <div className="form-floating mb-3">
                <input
                  onChange={handleChangeAutomobileVin}
                  required
                  placeholder="vin"
                  type="text"
                  id="autoVin"
                  name="autoVin"
                  className="form-control"
                  value={automobileVin}
                />
                <label htmlFor="autoVin">Vin</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={handleChangeCustomer}
                  required
                  placeholder="Customer"
                  type="text"
                  id="customer"
                  name="customer"
                  className="form-control"
                  value={customer}
                />
                <label htmlFor="customer">Customer</label>
              </div>

              {/* <div className="form-floating mb-3">
                <input
                  onChange={handleChangeEmployeeId}
                  required
                  placeholder="employeeId"
                  type="text"
                  id="employeeId"
                  name="employeeId"
                  className="form-control"
                  value={employeeId}
                />
                <label htmlFor="employeeId">Employee Id</label>
              </div> */}
              <div className="form-floating mb-3">
                <input
                  onChange={handleChangeDateTime}
                  required
                  type="datetime-local"
                  id="dateTime"
                  name="dateTime"
                  className="form-control"
                  value={dateTime}
                />
                <label htmlFor="dateTime">Date and Time</label>
              </div>

              <div className="mb-3">
                <select
                  onChange={handleChangeTechnician}
                  value={technician.id}
                  required
                  name="technician"
                  id="technician"
                  className="form-select"
                >
                  <option value=" ">Choose a technician</option>
                  {technicians.map((technician) => {
                    return (
                      <option key={technician.id} value={technician.id}>
                        {technician.first_name}
                      </option>
                    );
                  })}
                </select>
                <div className="form-floating mb-3">
                  <input
                    onChange={handleChangeReason}
                    required
                    placeholder="Reason"
                    type="text"
                    id="reason"
                    name="reason"
                    className="form-control"
                    value={reason}
                  />
                  <label htmlFor="reason">Reason</label>
                </div>
              </div>
              <button className="btn btn-lg create-button">Create</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}

export default CreateServiceAppointmentForm;
