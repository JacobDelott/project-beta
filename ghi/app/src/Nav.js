import { NavLink } from "react-router-dom";


function Nav() {


  return (
    <nav className="navbar navbar-dark bg-success">
<div className="container-fluid">
  <NavLink className="navbar-brand" to="/">
    CarCar
  </NavLink>
  <button
    className="navbar-toggler"
    type="button"
    data-bs-toggle="collapse"
    data-bs-target="#navbarSupportedContent"
    aria-controls="navbarSupportedContent"
    aria-expanded="false"
    aria-label="Toggle navigation"
  >
    <span className="navbar-toggler-icon"></span>
  </button>
  <div
    className="collapse navbar-collapse"
    id="navbarSupportedContent"
  >
    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
      <li className="nav-item">
        <NavLink className="nav-link active" to="/manufacturers">
          Manufacturers
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link active" to="/manufacturers/new">
           - Create a Manufacturer
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link active" to="/models">
          Models
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/models/create"
        >
          - Create a Model
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/automobiles"
        >
          Automobiles
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/automobiles/create"
        >
         - Create an Automobile
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/salespeople"
        >
          Salespeople
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/salespeople/new"
        >
         - Add a Salesperson
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/customers"
        >
          Customers
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink
          className="nav-link active"
          aria-current="page"
          to="/customers/new"
        >
         - Add a Customer
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link active" to="/sales">
          Sales
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link active" to="/sales/new">
         - Add a Sale
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link active" to="/salespersonhistory">
         - Salesperson History
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link active" to="/technicians">
          Technicians
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link active" to="/technicians/new">
         - Create a Technician
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link active" to="/appointments">
          Service Appointments
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link active" to="/appointments/history">
         - Service History
        </NavLink>
      </li>
      <li className="nav-item">
        <NavLink className="nav-link active" to="/appointments/create">
         - Create a Service Appointment
        </NavLink>
      </li>
    </ul>
  </div>
</div>
</nav>
  );
}

export default Nav;
