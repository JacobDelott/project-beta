# CarCar

Team:

- Jacob Delott - Service
- Jaime Huey - Sales

## How to Run this App

1. Fork this repository

2. Clone the forked repository onto local computer:
   git clone <REPOSITORYURL>

3. Build and run the project using Docker with these commands:
```
   docker volume create beta-data
   docker-compose build
   docker-compose up
```

4. Make sure all Docker containers are running

5. View project in the browser:
   http://localhost:3000/

   The links are can be accessed in the navigation dropdown links.


## Design and Diagram

![Img](carCarfinal.png)

## Inventory microservice

The Inventory microservice is the inventory of all cars, sold and unsold. This is where all automobile information is stored for sales and services provided. You will first need to create a Manufacturer, then the Model, and finally the car color, year, and VIN, with an existing model ID.

Please follow the directions in the following Learn URL, and create an automobile before attempting to create any Sales or Services:

- Learn URL: https://learn-2.galvanize.com/cohorts/3727/blocks/1890/content_files/build/01-practice-test-project/66-assessment-project.md

You can also create the Manufacturer, Model, and Automobile in the browser using the navigation links:
http://localhost:3000/

## Service microservice

The Service microservice has 3 models: AutomobileVO,Technician, and Appointment. The AutomobileVO is a value object that collects information from the Inventory API model Automobile using the poller. Using the Automobile model, we are able to pull the car VIN, and the sold status of the vehicle. We also collect the Technician and Appointment. Along with the AutomobileVO details, we are able to create technician, and appointment by inputting the technician information such as First Name, Last Name, and Employee Id. As well can create a service appointment by inputing the Vin, Customer, Employee Id, Date and Time, Reason, and choose a Technician from the dropdown option. Once the technician is created we can view a list of the technicians in the "Technicians" list table. As well once the technician is created it populates the the drop down option in the "Choose a technician" field in the Create a Service Appointment. Once the Service Appointment is created you can see a list of Service Appointments in the link called "Service Appointments." Once inside the "Service Appointments" UI, you'll see a table that lists all the created appointments.There is an option for each appointment that has a button to initialize a cancel or finish to reference the status of the appointment. When either the cancel or finish button is chosen, that appointment then gets cleared from the "Service Appointments," table and sent to the "Service History," UI. Inside the "Service History,'' you'll see all the created appointments and the the status.

Technician Endpoints:
List Technicians | GET | http://localhost:8080/api/technicians/
Create Technician | POST | http://localhost:8080/api/technicians/
Delete a technician | DELETE | http://localhost:8080/api/technicians/<int:id>/

JSON data examples:
List Technicians | GET | http://localhost:8080/api/technicians/
This is an example of what should be returned when successfully listing the Technicians.
```
{
"technicians": [
{
"first_name": "Lisa",
"last_name": "Simpson",
"employee_id": "1",
"id": 1
},
{
"first_name": "Bart",
"last_name": "Smith",
"employee_id": "2",
"id": 5
},
{
"first_name": "moe",
"last_name": "Syzlak",
"employee_id": "4",
"id": 6
},
{
"first_name": "Marge",
"last_name": "Simpson",
"employee_id": "6",
"id": 7
}
]
}
```
Create Technician | POST | http://localhost:8080/api/technicians/
Here is a sample to test the POST request to create a technician
```
{
"first_name": "moe",
"last_name": "Syzlak",
"employee_id": "4"

}
```
Delete a technician | DELETE | http://localhost:8080/api/technicians/<int:id>/
Here is an example to test the DEL request. User must provide matching Id to url to initialize correct deletion endpoint.
```
{
"id": null,
"first_name": "Test",
"last_name": "test",
"employee_id": "test"
}
```
Appointment Endpoints:
List Appointments | GET | http://localhost:8080/api/appointments/
Create Appointment | POST | http://localhost:8080/api/appointments/
Delete Appointment | DEL | http://localhost:8080/api/appointments/id/

JSON data examples:
Create Appointment | POST | http://localhost:8080/api/appointments/
Here is some sample Json to test the Create Appointment Post request.
```
{
"date_time": "2023-07-25T19:52:56.107908+00:00",
"reason": "oil change",
"status": "",
"vin": "AB9CY2345EF998",
"customer": "Nancy Skinner",
"technician": 1

}
```
List Appointments | GET | http://localhost:8080/api/appointments/
Here is some sample Json to test the List Appointments GET request
```
{
"appointments": [
{
"date_time": "2023-07-26T00:02:02.921050+00:00",
"reason": "oil change",
"status": "finished",
"vin": "AB6CD2345EF678GHI",
"customer": "Jake Delott",
"id": 2,
"is_vip": false,
"technician": "Lisa"
}
]
}
```
Delete Appointment | DEL | http://localhost:8080/api/appointments/id/
Here is the sample Json when deleting an appointment. The user must match the id to the url to delete the correct appointment.
```
{
"date_time": "2023-07-25T22:24:07.929610+00:00",
"reason": "oil change",
"status": "finished",
"vin": "AB6CD2345EF678GHI",
"customer": "Jake Delott",
"id": null,
"technician": "Lisa"
}
```
## Service microservice ## Value Objects:

The AutomobileVO is the value object used for the Service microservice. This VO gathered data from the Inventory API model Automobile, collecting the VIN and sold status of all vehicles in the inventory using the poller.

The VIN is the unique identifier for the automobiles url, which is where we are able to update the sold status to True when a sale is recorded. This is important because we can only sell a vehicle once, and only unsold (sold=false) vehicles will appear in the sale dropdown of cars.

## Sales microservice

The Sales microservice has 4 models: Salesperson, Customer, Sale, and AutomobileVO. The AutomobileVO is a value object that collects information from the Inventory API model Automobile using the poller. Using the Automobile model, we are able to pull the car VIN, and the sold status of the vehicle. We also collect the Salesperson and Customer information. Along with the AutomobileVO details, we are able to create a Sale by inputting the Salesperson from a dropdown, customer from a dropdown, the unsold vehicle via the VIN, and the price it was sold at. We are able to record the sale with all of the details, and then filter the sales history by each Salesperson.

## Sales microservice ## Salesperson:

The Salesperson model collects identifying information of the car salesperson with the first name, last name, and employee ID.

Insomnia details:
List salespeople - GET - http://localhost:8090/api/salespeople/
Create salesperson - POST - http://localhost:8090/api/salespeople/
Delete salesperson - DELETE - http://localhost:8090/api/salespeople/<id>/

Create salesperson - POST - http://localhost:8090/api/salespeople/
To Create salesperson (POST), enter this Json body:
```
    {
        "first_name": "Jaime",
        "last_name": "H",
        "employee_id": 528
    }
```
List salespeople - GET - http://localhost:8090/api/salespeople/
To List salespeople (GET), the fields of each salesperson will appear like this, and include a unique ID for each salesperson:
```
{
"salespeople": [
{
"id": 1,
"first_name": "Oliver",
"last_name": "H",
"employee_id": "608"
},
{
"id": 9,
"first_name": "Ellie",
"last_name": "H",
"employee_id": "8"
},
{
"id": 10,
"first_name": "Jaime",
"last_name": "H",
"employee_id": "528"
}
]
}
```

Delete salesperson - DELETE - http://localhost:8090/api/salespeople/<id>/
To Delete salesperson (DELETE), simply enter the unique ID in the URL. Upon deletion the result will show id as null, and the salesperson will be removed from the list:
* Please note: If you try to delete a salesperson that has a sales history, the salesperson will be protected.
```
{
"id": null,
"first_name": "Test",
"last_name": "test",
"employee_id": "test"
}
```

## Sales microservice ## Customer:

The Customer model collects identifying information about the customer, including the first name, last name, address, and phone number.

Insomnia details:
List customer - GET - http://localhost:8090/api/customers/
Create customer - POST - http://localhost:8090/api/customers/
Delete customer - DELETE - http://localhost:8090/api/customers/<id>/

Create customer - POST - http://localhost:8090/api/customers/
To Create a customer (POST), enter this Json body:
```
		{
			"first_name": "Jaime",
			"last_name": "Huey",
			"address": "123 Home St",
			"phone_number": 123456789
		}
```

List customer - GET - http://localhost:8090/api/customers/
To List the customers (GET), the fields of each customer will appear like this, with a unique ID for each customer:
```
{
"customers": [
{
"id": 5,
"first_name": "Jaime",
"last_name": "Huey",
"address": "1234 Main St",
"phone_number": "789456123"
},
{
"id": 6,
"first_name": "Ellie",
"last_name": "Huey",
"address": "123 Kitten Town",
"phone_number": "12121212"
}
]
}
```
Delete customer - DELETE - http://localhost:8090/api/customers/<id>/
To Delete customer (DELETE), simply enter the unique ID in the URL. Upon deletion the result will show id as null, and the customer will be removed from the list:
* Please note: If you try to delete a customer that has a sales history, the customer will be protected.
```
{
"id": null,
"first_name": "Jaime",
"last_name": "Huey",
"address": "123 Home St",
"phone_number": "123456789"
}
```

## Sales microservice ## Sale:

The Sale model records the sale of a vehicle, collecting the price the vehicle was sold at, as well as the vehicle itself identified by its VIN through the AutomobileVO foreign key, the Salesperson foreign key, and the Customer foreign key. The automobile, salesperson, and customer field inputs require the unique ID of the existing data.

Insomnia details:
List sales - GET - http://localhost:8090/api/sales/
Create sale - POST - http://localhost:8090/api/sales/
Delete sale - DELETE - http://localhost:8090/api/sales/<id>/

Create sale - POST - http://localhost:8090/api/sales/
To Create a sale (POST), enter this Json body:
* Please note: You will need to have created an existing automobile, salesperson, and customer with the ID number input below.
* Your price can only include numbers.
```
{
"price": 60000,
"automobile": 1,
"salesperson": 1,
"customer": 1
}
```

List sales - GET - http://localhost:8090/api/sales/
To List the sales (GET), the fields of each sale will appear like this, creating a unique ID for each sale, identifying the automobile by its VIN, the salesperson by first and last name, the salesperson employee id, the customer by first and last name, and changing the sold value to true:
```
{
"sales": [
{
"id": 6,
"price": 50000,
"automobile": "1C3CC5FB2AN120456",
"salesperson": "Oliver H",
"customer": "Test Last",
"salesperson_id": "608",
"sold": true
},
{
"id": 7,
"price": 50000,
"automobile": "1C3CC5FB2AN120174",
"salesperson": "Oliver H",
"customer": "Test Last",
"salesperson_id": "608",
"sold": true
},
{
"id": 8,
"price": 50000,
"automobile": "1C3CC5FB2AN120174",
"salesperson": "Oliver H",
"customer": "Jaime Huey",
"salesperson_id": "608",
"sold": true
}
]
}
```

Delete sale - DELETE - http://localhost:8090/api/sales/<id>/
To Delete a sale (DELETE), simply enter the unique ID in the URL. Upon deletion the result will show id as null, and the sale will be removed from the list:
```
{
"id": null,
"price": 50000,
"automobile": "1C3CC5FB2AN120174",
"salesperson": "jaime huey",
"customer": "Jaime Huey",
"salesperson_id": "jhuey",
"sold": true,
"salespersonID": 11
}
```

## Sales microservice ## Value Objects:

The AutomobileVO is the value object used for the Sales microservice. This VO gathered data from the Inventory API model Automobile, collecting the VIN and sold status of all vehicles in the inventory using the poller.

The VIN is the unique identifier for the automobiles url, which is where we are able to update the sold status to True when a sale is recorded. This is important because we can only sell a vehicle once, and only unsold (sold=false) vehicles will appear in the sale dropdown of cars.
